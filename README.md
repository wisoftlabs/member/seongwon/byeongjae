# You byungjae fan homepages

[![Version](https://img.shields.io/badge/version-2019.08.19-red.svg)](./CHANGELOG)  [![License](https://img.shields.io/github/license/mashape/apistatus.svg)](./LICENSE)

![](./logo.bmp)

본 프로젝트는 크리에이터들과 함께 세상을 즐겁게 만들어 나가는 MCN 업계 대표 디지털 엔터테인먼트 기업 SANDBOX 소속인 유병재를 위한 프로필 페이지 제작을 위해 만들어졌습니다. 

본 홈페이지는 소속사와는 관련이 없으며, 본인이 자발적으로 제작되었습니다. 오픈소스 기반으로 누구나 자유롭게 수정이 가능하나 배포시 사용자에게 반드시 허락을 받아야 합니다. 

[![SANDBOX NETWORK](https://platum.kr/wp-content/uploads/2018/09/aaa.png)](https://sandbox.co.kr/)



## Getting Started

* 프로젝트를 실행하기 위해서는 **반드시** Docker가 설치되어 있어야 합니다. 

* Docker 설치 방법은 공식 Documents를 확인하여 설치하여 주시길 바랍니다. 

  

### Docker build install

* 소스를 빌드하여 사용하시는 경우 다음과 같은 방법을 사용하세요.

```bash
$ git clone https://git.wisoft.io/seongwon/byeongjae.git
```

```bash
$ docker buid -t <이미지 이름>/<태그> <경로> 
```

```bash
$ docker run -p 8888:80 -d $(docker-images)
```



### Docker Hub 

본 프로젝트는 Docker Hub에도 업로드 되어 있습니다. 사용방법은 다음과 같습니다.

```bash
$ docker pull jusk2/seongwon-byungjae:<version> 
```

```bash
$ docker run -p 8888:80 -d jusk2/seongwon-byungjae:<version> 
```

* Docker Hub에 등록된 이미지는 누구나 다운로드 하여 사용할 수 있습니다. 



## Developer Profile

* seongwon
  * 블로그 :  https://judo0179.tistory.com/
  * Github : https://github.com/pandora0667
  * Instagram : https://www.instagram.com/seongwon0667/



## 홈페이지 주소

* https://dbqudwo333.ml

도메인은 freemon을 사용했으며, SSL 인증서는 Let's Encrypt을 사용했습니다. 

악의적인 해킹 및 공격은 불법이며, 접속 로그를 통해 추적가능함을 알려드립니다. 



## License

This project is licensed under the MIT License - see the [LICENSE.md](https://git.wisoft.io/seongwon/byeongjae/blob/master/LICENSE) file for details



## Acknowledgments

* Docker 
* BootStrap 